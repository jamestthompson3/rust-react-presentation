export async function wasmUtils() {
  const wasmUtils = await import("../wasm_magic");
  const wasmBuffers = await import("../wasm_magic_bg");
  return { ...wasmUtils, ...wasmBuffers };
}
