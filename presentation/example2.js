import React, { useState } from "react";
import { Heading, Text } from "spectacle";

import styles from "./index.css";
import text from "./text.json";
import { Universe } from "./universe";
import { wasmUtils } from "./wasmIndex";

const jsverse = new Universe();

function TickPerf({ active, tools }) {
  const universe = tools.Universe.new();
  const start = window.performance.now();
  active === "js" ? jsverse.tick() : universe.tick();
  const end = window.performance.now();
  const time = end - start;
  return (
    <div>
      <p>
        <strong>Tick Time:</strong> {time.toFixed(4)} ms
      </p>
    </div>
  );
}

export default class Example2 extends React.Component {
  state = {
    active: null,
    wasmTools: null
  };

  componentDidMount() {
    wasmUtils().then(tools => this.setState({ wasmTools: tools }));
  }

  render() {
    const { active, wasmTools } = this.state;
    const activeStyles = {
      border: "5px solid cornflowerblue",
      borderRadius: 5
    };
    return (
      <div>
        <Heading style={{ margin: 0 }}>Performance</Heading>
        <br />
        <br />
        <div className="grep-test">
          <img
            src="https://upload.wikimedia.org/wikipedia/commons/thumb/d/d5/Rust_programming_language_black_logo.svg/2000px-Rust_programming_language_black_logo.svg.png"
            className="grep-image"
            style={active === "rust" ? activeStyles : {}}
            onClick={() => this.setState({ active: "rust" })}
          />
          <img
            src="https://upload.wikimedia.org/wikipedia/commons/6/6a/JavaScript-logo.png"
            className="grep-image"
            style={active === "js" ? activeStyles : {}}
            onClick={() => this.setState({ active: "js" })}
          />
        </div>

        <p>5000 x 5000 tiles</p>
        <p>25 000 000 individual cells</p>
        {wasmTools && active && <TickPerf active={active} tools={wasmTools} />}
      </div>
    );
  }
}
