import React, { useState } from "react";
import { Heading, Text } from "spectacle";

import styles from "./index.css";
import text from "./text.json";
import { wasmUtils } from "./wasmIndex";

function jsSearch(body, term) {
  return body.split(".").filter(sentence => sentence.includes(term))
}

function wasmSearch(body, term, tools) {
  return tools.search_string(body, term);
}

function TextGrep({ searchTerm, active, tools }) {
  const body = text.text.join("");
  if (!searchTerm || !active) {
    return (
      <div className="text-container">
        <p>{text.text}</p>
      </div>
    );
  } else {
    const start = window.performance.now();
    const matches =
      active === "js"
        ? jsSearch(body, searchTerm)
        : wasmSearch(body, searchTerm, tools);
    const end = window.performance.now();
    const time = end - start;
    return (
      <div>
        <p>
          <strong>Search Time:</strong> {time.toFixed(4)} ms
          <br />
          <strong>Matches:</strong> {matches.length}
        </p>
        <div className="text-container">
          {matches.map((match, i) => <p key={i}>{match}</p>)}
        </div>
      </div>
    );
  }
}

export default class Example1 extends React.Component {
  state = {
    searchTerm: null,
    active: null,
    wasmTools: null
  };

  componentDidMount() {
    wasmUtils().then(tools => this.setState({ wasmTools: tools }));
  }

  handleChange = e => {
    this.setState({ searchTerm: e.target.value });
  };

  render() {
    const activeStyles = {
      border: "5px solid cornflowerblue",
      borderRadius: 5
    };
    const { searchTerm, active, wasmTools } = this.state;
    return (
      <div>
        <Heading style={{ margin: 0 }}>Example 1</Heading>
        <br />
        <br />
        <div className="grep-test">
          <img
            src="https://upload.wikimedia.org/wikipedia/commons/thumb/d/d5/Rust_programming_language_black_logo.svg/2000px-Rust_programming_language_black_logo.svg.png"
            className="grep-image"
            style={active === "rust" ? activeStyles : {}}
            onClick={() => this.setState({ active: "rust" })}
          />
          <img
            src="https://upload.wikimedia.org/wikipedia/commons/6/6a/JavaScript-logo.png"
            className="grep-image"
            style={active === "js" ? activeStyles : {}}
            onClick={() => this.setState({ active: "js" })}
          />

          <input
            onChange={this.handleChange}
            className="input-container"
            placeholder="search the text"
          />
        </div>
        <br />
        <br />
        {wasmTools && (
          <TextGrep active={active} searchTerm={searchTerm} tools={wasmTools} />
        )}
      </div>
    );
  }
}
