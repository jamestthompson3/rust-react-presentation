import React from "react";
import { Heading, Text } from "spectacle";

import { wasmUtils } from "./wasmIndex";
const CELL_SIZE = 5;
const GRID_COLOR = "#CCCCCC";
const DEAD_COLOR = "#FFFFFF";
const ALIVE_COLOR = "#000000";

// These must match `Cell::Alive` and `Cell::Dead` in `src/lib.rs`.
const DEAD = 0;
const ALIVE = 1;

const width = 500;
const height = 500;

export default class GameOfLife extends React.Component {
  state = {
    tools: null
  };

  componentDidMount() {
    wasmUtils().then(tools => this.setState({ tools }));
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.state.tools) {
      this.draw();
    }
  }
  draw = () => {
    const { tools } = this.state;
    const universe = tools.Universe.new();

    const ctx = this.canvas && this.canvas.getContext("2d");

    const renderLoop = () => {
      universe.tick();

      drawCells();
      drawGrid();

      requestAnimationFrame(renderLoop);
    };

    const drawGrid = () => {
      ctx.beginPath();
      ctx.lineWidth = 1 / window.devicePixelRatio;
      ctx.strokeStyle = GRID_COLOR;

      // Vertical lines.
      for (let i = 0; i <= width; i++) {
        ctx.moveTo(i * (CELL_SIZE + 1) + 1, 0);
        ctx.lineTo(i * (CELL_SIZE + 1) + 1, (CELL_SIZE + 1) * height + 1);
      }

      // Horizontal lines.
      for (let j = 0; j <= height; j++) {
        ctx.moveTo(0, j * (CELL_SIZE + 1) + 1);
        ctx.lineTo((CELL_SIZE + 1) * width + 1, j * (CELL_SIZE + 1) + 1);
      }

      ctx.stroke();
    };

    const getIndex = (row, column) => {
      return row * width + column;
    };

    const drawCells = () => {
      const cellsPtr = universe.cells();
      const cells = new Uint8Array(
        tools.memory.buffer,
        cellsPtr,
        width * height
      );

      ctx.beginPath();

      for (let row = 0; row < height; row++) {
        for (let col = 0; col < width; col++) {
          const idx = getIndex(row, col);

          ctx.fillStyle = cells[idx] === DEAD ? DEAD_COLOR : ALIVE_COLOR;

          ctx.fillRect(
            col * (CELL_SIZE + 1) + 1,
            row * (CELL_SIZE + 1) + 1,
            CELL_SIZE,
            CELL_SIZE
          );
        }
      }

      ctx.stroke();
    };

    requestAnimationFrame(renderLoop);
  };

  render() {
    const { tools } = this.state;
    return (
      <div>
        <Heading style={{ margin: 0 }}>Example 2</Heading>
        <br />
        <br />
        <div className="start-button">Game of Life</div>
        {tools && (
          <canvas ref={ref => (this.canvas = ref)} width={500} height={500} />
        )}
      </div>
    );
  }
}
