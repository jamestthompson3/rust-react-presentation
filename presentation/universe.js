function getCellStatus(cell, liveNeighbors) {
  switch (true) {
    case cell === 1 && liveNeighbors < 2:
      return 0;
    case (cell === 1 && liveNeighbors === 2) ||
      (cell === 1 && liveNeighbors === 3):
      return 1;
    case cell === 1 && liveNeighbors > 3:
      return 0;
    case cell === 0 && liveNeighbors === 3:
      return 1;
    default:
      break;
  }
}

export class Universe {
  constructor() {
    this.width = 5000;
    this.height = 5000;
    this.cells = new Array(this.width * this.height)
      .fill(null)
      .map((n, idx) => (idx % 2 === 0 || idx % 7 === 0 ? 1 : 0));
    this.tick = this.tick.bind(this);
    this.getIndex = this.getIndex.bind(this);
    this.liveNeighborCount = this.liveNeighborCount.bind(this);
  }

  getIndex(row, column) {
    return row * this.width + column;
  }

  liveNeighborCount(row, column) {
    let count = 0;
    const north = row === 0 ? this.height - 1 : row - 1;
    const south = row === this.height - 1 ? 0 : row + 1;
    const west = column === 0 ? this.width - 1 : column - 1;
    const east = column === this.width - 1 ? 0 : column + 1;
    const nw = this.getIndex(north, west);
    count += this.cells[nw];
    const n = this.getIndex(north, column);
    count += this.cells[n];
    const ne = this.getIndex(north, east);
    count += this.cells[ne];
    const w = this.getIndex(row, west);
    count += this.cells[w];
    const e = this.getIndex(row, east);
    count += this.cells[e];
    const sw = this.getIndex(south, west);
    count += this.cells[sw];
    const s = this.getIndex(south, column);
    count += this.cells[s];
    const se = this.getIndex(south, east);
    count += this.cells[se];
    return count;
  }

  tick() {
    const next = this.cells;
    for (let row = 0; this.height; row++) {
      for (let col = 0; this.width; col++) {
        const idx = this.getIndex(row, col);
        const cell = this.cells[idx];
        const liveNeighbors = this.liveNeighborCount(row, col);
        const nextCell = getCellStatus(cell, liveNeighbors);
        next[idx] = nextCell;
      }
    }
  }
}
