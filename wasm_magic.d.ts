/* tslint:disable */
export function search_string(arg0: string, arg1: string): any;

export class Universe {
free(): void;

static  new(): Universe;

 width(): number;

 height(): number;

 cells(): number;

 tick(): void;

 toggle_cell(arg0: number, arg1: number): void;

}
export class Options {
free(): void;

}
export class KDBush {
free(): void;

}
